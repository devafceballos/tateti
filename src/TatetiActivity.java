import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class TatetiActivity extends JFrame {

    private JFrame mJframe;
    private JPanel mPanel;
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;
    private JButton button5;
    private JButton button6;
    private JButton button7;
    private JButton button8;
    private JButton button9;
    private ArrayList <JButton> tablero;
    private int turnoCont;
    private Pieza playerX;
    private Pieza playerO;

    public TatetiActivity(){
    mJframe = new JFrame("TATETI");
    createMainFrame(mJframe, mPanel);
    mJframe.setSize(500,550);
    turnoCont = 1;
    playerX = new Pieza();
    playerX.setmTipoPieza("X");
    playerO = new Pieza("O");

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (turnoCont % 2 == 0){
                    button1.setText(playerX.getmTipoPieza());
                    turnoCont++;
                    button1.setEnabled(false);
                }
                else {
                    button1.setText(playerO.getmTipoPieza());
                    turnoCont++;
                    button1.setEnabled(false);
                }
                winner();
            }
        });

        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (turnoCont % 2 == 0){
                    button2.setText(playerX.getmTipoPieza());
                    turnoCont++;
                    button2.setEnabled(false);
                }
                else {
                    button2.setText(playerO.getmTipoPieza());
                    turnoCont++;
                    button2.setEnabled(false);
                }
                winner();
            }
        });

        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (turnoCont % 2 == 0){
                    button3.setText(playerX.getmTipoPieza());
                    turnoCont++;
                    button3.setEnabled(false);
                }
                else {
                    button3.setText(playerO.getmTipoPieza());
                    turnoCont++;
                    button3.setEnabled(false);
                }
                winner();
            }
        });

        button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (turnoCont % 2 == 0 ){
                    button4.setText(playerX.getmTipoPieza());
                    turnoCont++;
                    button4.setEnabled(false);
                }

                else {
                    button4.setText(playerO.getmTipoPieza());
                    turnoCont++;
                    button4.setEnabled(false);
                }
                winner();

            }
        });

        button5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (turnoCont % 2 == 0){
                    button5.setText(playerX.getmTipoPieza());
                    turnoCont++;
                    button5.setEnabled(false);
                }
                else {
                    button5.setText(playerO.getmTipoPieza());
                    turnoCont++;
                    button5.setEnabled(false);
                }
                winner();
            }
        });

        button6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (turnoCont % 2 == 0){
                    button6.setText(playerX.getmTipoPieza());
                    turnoCont++;
                    button6.setEnabled(false);
                }
                else {
                    button6.setText(playerO.getmTipoPieza());
                    turnoCont++;
                    button6.setEnabled(false);
                }
                winner();
            }
        });

        button7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (turnoCont % 2 == 0){
                    button7.setText(playerX.getmTipoPieza());
                    turnoCont++;
                    button7.setEnabled(false);
                }

                else {
                    button7.setText(playerO.getmTipoPieza());
                    turnoCont++;
                    button7.setEnabled(false);
                }
                winner();
            }
        });

        button8.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (turnoCont % 2 == 0){
                    button8.setText(playerX.getmTipoPieza());
                    turnoCont++;
                    button8.setEnabled(false);
                }
                else {
                    button8.setText(playerO.getmTipoPieza());
                    turnoCont++;
                    button8.setEnabled(false);
                }
                winner();
            }
        });

        button9.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (turnoCont % 2 == 0){
                    button9.setText(playerX.getmTipoPieza());
                    turnoCont++;
                    button9.setEnabled(false);
                }
                else {
                    button9.setText(playerO.getmTipoPieza());
                    turnoCont++;
                    button9.setEnabled(false);
                }
                winner();
            }
        });
        // ArrayList de botones
        tablero = new ArrayList<>();
        tablero.add(button1);
        tablero.add(button2);
        tablero.add(button3);
        tablero.add(button4);
        tablero.add(button5);
        tablero.add(button6);
        tablero.add(button7);
        tablero.add(button8);
        tablero.add(button9);
    }
    // Metodo maester check
    public void winner() {
        //WIN PLAYER X
        if (tablero.get(0).getText().equals("X") && tablero.get(1).getText().equals("X") && tablero.get(2).getText().equals("X")) {
            enableButton();
            winPlayerX();
            turnoCont++;
        }

        if (tablero.get(3).getText().equals("X") && tablero.get(4).getText().equals("X") && tablero.get(5).getText().equals("X")) {
            enableButton();
            winPlayerX();
            turnoCont++;
        }

        if (tablero.get(6).getText().equals("X") && tablero.get(7).getText().equals("X") && tablero.get(8).getText().equals("X")) {
            enableButton();
            winPlayerX();
            turnoCont++;
        }

        if (tablero.get(0).getText().equals("X") && tablero.get(4).getText().equals("X") && tablero.get(8).getText().equals("X")) {
            enableButton();
            winPlayerX();
            turnoCont++;
        }
        if (tablero.get(2).getText().equals("X") && tablero.get(4).getText().equals("X") && tablero.get(6).getText().equals("X")) {
            enableButton();
            winPlayerX();
            turnoCont++;
        }

        if (tablero.get(0).getText().equals("X") && tablero.get(3).getText().equals("X") && tablero.get(6).getText().equals("X")) {
            enableButton();
            winPlayerX();
            turnoCont++;
        }

        if (tablero.get(1).getText().equals("X") && tablero.get(4).getText().equals("X") && tablero.get(7).getText().equals("X")) {
            enableButton();
            winPlayerX();
            turnoCont++;
        }

        if (tablero.get(2).getText().equals("X") && tablero.get(5).getText().equals("X") && tablero.get(8).getText().equals("X")) {
            enableButton();
            winPlayerX();
            turnoCont++;
        }

        // WIN PLAYER "O"
        if (tablero.get(0).getText().equals("O") && tablero.get(1).getText().equals("O") && tablero.get(2).getText().equals("O")) {
            enableButton();
            winPlayerO();
            turnoCont++;
        }

        if (tablero.get(3).getText().equals("O") && tablero.get(4).getText().equals("O") && tablero.get(5).getText().equals("O")) {
            enableButton();
            winPlayerO();
            turnoCont++;
        }

        if (tablero.get(6).getText().equals("O") && tablero.get(7).getText().equals("O") && tablero.get(8).getText().equals("O")) {

            enableButton();
            winPlayerO();
            turnoCont++;
        }

        if (tablero.get(0).getText().equals("O") && tablero.get(4).getText().equals("O") && tablero.get(8).getText().equals("O")) {

            enableButton();
            winPlayerO();
            turnoCont++;
        }
        if (tablero.get(2).getText().equals("O") && tablero.get(4).getText().equals("O") && tablero.get(6).getText().equals("O")) {

            enableButton();
            winPlayerO();
            turnoCont++;
        }

        if (tablero.get(0).getText().equals("O") && tablero.get(3).getText().equals("O") && tablero.get(6).getText().equals("O")) {

            enableButton();
            winPlayerO();
            turnoCont++;
        }

        if (tablero.get(1).getText().equals("O") && tablero.get(4).getText().equals("O") && tablero.get(7).getText().equals("O")) {

            enableButton();
            winPlayerO();
            turnoCont++;
        }

        if (tablero.get(2).getText().equals("O") && tablero.get(5).getText().equals("O") && tablero.get(8).getText().equals("O")) {

            enableButton();
            winPlayerO();
            turnoCont++;
        }
    }

    public void enableButton() {
        tablero.get(0).setEnabled(false);
        tablero.get(1).setEnabled(false);
        tablero.get(2).setEnabled(false);
        tablero.get(3).setEnabled(false);
        tablero.get(4).setEnabled(false);
        tablero.get(5).setEnabled(false);
        tablero.get(6).setEnabled(false);
        tablero.get(7).setEnabled(false);
        tablero.get(8).setEnabled(false);
    }

    public void winPlayerX(){
        tablero.get(0).setText(" ");
        tablero.get(1).setText("YOU");
        tablero.get(2).setText(" ");
        tablero.get(3).setText("W");
        tablero.get(4).setText("I");
        tablero.get(5).setText("N");
        tablero.get(6).setText(" ");
        tablero.get(7).setText("player X!");
        tablero.get(8).setText(" ");
    }

    public void winPlayerO() {
        tablero.get(0).setText(" ");
        tablero.get(1).setText("YOU");
        tablero.get(2).setText(" ");
        tablero.get(3).setText("W");
        tablero.get(4).setText("I");
        tablero.get(5).setText("N");
        tablero.get(6).setText(" ");
        tablero.get(7).setText("player O!");
        tablero.get(8).setText(" ");
    }

    public void createMainFrame(JFrame frame, JPanel panel){
        frame.getContentPane().add(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
